# CIGALE manual v1.0
This manual (CIGALE\_Manual.pdf) serves as a "quick and practical" reference for the user.
Further questions can be asked in our discussion forum (https://github.com/mboquien/cigale/discussions).
All materials of CIGALE (including this manual) can be found on https://cigale.lam.fr.

The last version of the manual is available [here](https://gitlab.lam.fr/cigale/manual/-/jobs/artifacts/master/raw/CIGALE_Manual.pdf?job=build).
